


function getPrintPath(relativePath)
{
	var code = window.winDiagConnList.getMapFig();
	var map  = window.winDiagConnList.getMapType();
	
	var dirName;
	if( map == "system" ){
		dirName = "print";
	}
	else{
		dirName = "pdf";
	}
	
	return relativePath + map + "/" + dirName + "/" + code + ".pdf";
}


function getOutlinePath(relativePath)
{
	var code = window.winDiagConnList.getMapFig();
	var map  = window.winDiagConnList.getMapType();
	
	return relativePath + map + "/outline/" + code + ".html";
}
